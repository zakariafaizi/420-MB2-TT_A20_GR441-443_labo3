package com.example.astronomy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;
import java.util.Random;

public class AstreCeleste extends Object
{

    Drawable kawkab;
    private int posX;
    private int posY;
    private Random rand2;
    private int counter;
//    private Bitmap kawkab;
    private int radius;
    private boolean status;
    public Context context;
    public ArrayList array;

    public static final int[] images = {R.drawable.planet1, R.drawable.planet2,R.drawable.planet3, R.drawable.planet4,R.drawable.planet5, R.drawable.planet6,R.drawable.planet7, R.drawable.planet8};


    public AstreCeleste()
    {


        status = true;
        rand2 = new Random();
        posX = rand2.nextInt(900);
        posY = rand2.nextInt(1200);
        counter = 0;




        radius = 10;

    }

    public class Drawab extends View
    {

        public Drawab(Context ctx)
        {
            super(ctx);

        }


        public Drawable Kaw()
        {

            kawkab = ResourcesCompat.getDrawable(getResources(), images[counter], null);
            counter++;
            return kawkab;
        }




    }

     public static Bitmap drawableToBitmap (Drawable drawable) {

      /*  if (drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable)drawable).getBitmap();
        }*/



        Bitmap bitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);


        return bitmap;
    }


    public boolean getStatus()
    {
        return this.status;
    }

    public void setStatus(Boolean state)
    {
        this.status = state;

       /* if(!this.status)
        {

        }*/
    }

    public ArrayList test(int i){
        if(!this.status)
        {
          array = new ArrayList();
            switch(i)
            {
                case 0 :
                    array.add("Mercury");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");
                    break;
                case 1 :
                    array.add("Venus");
                    array.add( "12,104 km" );
                    array.add( "108,209,475 km");
                    array.add("225 Earth days");
                    break;
                case 2 :

                    array.add("Earth");
                    array.add( "12,742 km" );
                    array.add( "0km");
                    array.add("365 days");
                    break;
                case 3 :
                    array.add("Mars");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");
                    break;
                case 4 :
                    array.add("Jupiter");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");
                    break;
                case 5 :
                    array.add("Saturn");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");
                    break;
                case 6 :
                    array.add("Uranus");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");
                    break;
                case 7 :
                    array.add("Neptune");
                    array.add( "4,879 km" );
                    array.add( "57,909,227km");
                    array.add("88 Earth days");

                    break;




            }

        }
        return array;

    }

    public int getPosX()
    {
        return this.posX;
    }

    public int getPosY()
    {
       return this.posY;
    }



    protected void placeImage(Canvas canv , Drawable kaw)
    {

        canv.drawBitmap(drawableToBitmap(kaw),posX,posY,null);

    }
}
