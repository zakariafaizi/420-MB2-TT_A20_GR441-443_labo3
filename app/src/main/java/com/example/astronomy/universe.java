package com.example.astronomy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class universe extends AppCompatActivity {

    private  AlienSolarSystem space;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        space = new AlienSolarSystem(this);
        setContentView(space);
    }
}