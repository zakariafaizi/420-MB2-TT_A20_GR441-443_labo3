package com.example.astronomy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    private RadioButton tactile;
    private RadioButton sensor;
    private Button start;
    private Context ctx;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ctx = this.getApplicationContext();

        tactile = findViewById(R.id.tactile);
        sensor = findViewById(R.id.sensor);
        start = findViewById(R.id.start);


        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if(tactile.isChecked())
                {

                    i = new Intent(ctx,universe.class);
                       startActivity(i);
                }


            }
        });
    }
}