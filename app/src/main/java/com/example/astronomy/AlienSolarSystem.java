package com.example.astronomy;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class AlienSolarSystem extends View
{


    private AlertDialog alert ;
    private Random rand;
    private int posX;
    private int posY;
    private float Radius;
    private AstreCeleste[] astrecelestes = new AstreCeleste[8];
    private ArrayList<Drawable>  planets;
    private int counter;
    private Context mcontext;
    private boolean end;
    private TextView name,diameter,length,distance;
    private Button ok;
    ArrayList array;
    public Dialog dialog2;


    public AlienSolarSystem(Context context) {
        super(context);
        mcontext = context;
        final Dialog dialog = new Dialog(this.mcontext);

        Window window = dialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        dialog.setContentView(R.layout.dialog_view);

        name = dialog.findViewById(R.id.name);
        diameter = dialog.findViewById(R.id.diameter);
        length = dialog.findViewById(R.id.length);
        distance = dialog.findViewById(R.id.distance);
        ok = dialog.findViewById(R.id.ok);



        dialog2 = dialog;

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();

            }
        });

        AstreCeleste astr = new AstreCeleste();
        AstreCeleste.Drawab drwb = astr.new Drawab(mcontext);
        planets = new ArrayList<Drawable>();

        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());
        planets.add(drwb.Kaw());


        end = false;
        counter = 0;
        rand = new Random();
        posX = rand.nextInt(900);
        posY = rand.nextInt(1200);



        for (int i = 0; i < 8; i++)
        {
            AstreCeleste temp = new AstreCeleste();

            astrecelestes[i] = temp;
        }

        Radius = 30;
    }



    Drawable Vaisseau = getResources().getDrawable(R.drawable.spatial,null);

    public static Bitmap drawableToBitmap (Drawable drawable) {


        Bitmap bitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(drawableToBitmap(Vaisseau),posX,posY,null);

        for(int i=0 ; i<8 ; i++)
        {
            astrecelestes[i].placeImage(canvas, planets.get(i));
        }

        if(counter>=8 && !end)
        {
            Toast.makeText(mcontext,"THE End",Toast.LENGTH_LONG).show();
            end = true;
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();

        boolean limitL,limitR,LimitU,LimitD = false;

        switch (action)
        {

            case MotionEvent.ACTION_MOVE:
                posX = touchX;
                posY = touchY;

                for(int i =0;i<5;i++)
                {
                    limitL =  posX > (astrecelestes[i].getPosX()-100);
                    limitR =  posX < (astrecelestes[i].getPosX()+100);
                    LimitU =  posY > (astrecelestes[i].getPosY()-100);
                    LimitD =  posY < (astrecelestes[i].getPosY()+100);

                    if(limitL && limitR && LimitD && LimitU )
                    {
                        if(astrecelestes[i].getStatus())
                        {
                            astrecelestes[i].setStatus(false);
                            array = new ArrayList();
                            array = astrecelestes[i].test(i);
                            if(array.get(0).toString() == "Earth")
                            {
                                name.setText("HABITÉ !!! \n \n \n Name : " + array.get(0).toString());
                            }
                            else
                            {
                                name.setText("Name : " + array.get(0).toString());
                            }

                            diameter.setText("Diameter : " +array.get(1).toString());
                            distance.setText("Distance from earth: " +array.get(2).toString());
                            length.setText("Length : " +array.get(3).toString());

                            dialog2.show();

                            counter++;
                        }


                        System.out.println(counter);

                    }

                }

                break;


        }
        invalidate();
        return true;
    }
}
